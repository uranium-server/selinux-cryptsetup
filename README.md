# SELinux and `cryptsetup` clash

After I've finally decided my `/media` partition to be `BTRFS RAID1` on the top of `LUKS2`, next step would be to setup automatic decryption of all
underlying drives in that RAID at the boot time, i. e., speaking in terms of `OpenRC` init system, --- in the `boot` runlevel. Also worth to note that
the decryption was provided by `dmcrypt` init service, which was installed alongside `cryptsetup`.

But, SELinux became the main problem of this whole operation. The first problem was kind of trivial --- I just had to create module allowing `lvm_t`
context (`cryptsetup` shares context with `lvm`) to operate on the `alg_socket` (I honestly still don't understand what this really is) and some other
rules:
```
allow lvm_t self:alg_socket { create bind setopt accept write read };
allow udev_t lvm_t:process { noatsecure rlimitinh siginh };
allow lvm_t kernel_t:key search;
allow lvm_t initrc_state_t:dir search;
allow lvm_t cgroup_t:dir search;
```
I also would like to notice that in some perspective I doubt the necessity of `noatsecure rlimitnh siginh` triad, as I see a lot of such denials and
can't see their effect.

After I was able to manually `close` and `open` my encrypted drives and checked that my configuration of `dmcrypt` works fine, I happily rushed to
reboot. But, it all ended up with need of manual interaction with the server, because `dmcrypt` hanged up the whole boot process: he failed to get
access to drives and as last resort was asking for the password. After I disabled password "authentication", the problem persisted but in slightly
clear way:
```
dmcrypt                   |WARNING!
dmcrypt                   |========
dmcrypt                   |Detected device signature(s) on /dev/sdb1. Proceeding further may damage existing data.
dmcrypt                   |
dmcrypt                   |Are you sure? (Type 'yes' in capital letters): Operation aborted.
```
I can't say for sure but in my opinion `dmcrypt` wasn't allow to access the drive at all. And, just for note, I never "typed `yes` in capital letters"
--- who knows what would be the consequences.

I left this problem for a while as school was pressuring --- after all, manual opening and mounting of these drives worked fine, so I sticked to that.

After I've investigated this problem deeply I noticed that there are some `audit` logs in the `dmesg` before the start of the `auditd` daemon, which
was on the `default` runlevel. I've tried to pull `auditd` on the `boot` runlevel but it wouldn't start due to SELinux again --- so I didn't bother
with it and continued on the main problem. Here is the denial (restored from my memory as I haven't saved it):
```
type=AVC msg=audit(…:…): avc: denied { create } … scontext=…:…:lvm_t tcontext=…:…:var_run_t tclass=dir
```

My first and working solution was to just allow `lvm_t` to create directory in the `var_run_t` context --- I installed module doing exactly that and
tested that it works fine on the already running system. But, the problem persisted during the boot --- I don't have any sources, but from my point of
view, SELinux loads modules not immediately on the kernel start but somewhat closer to the `default` runlevel, so my module just didn't have time to
load before the `dmcrypt` turn. Already a bit desperate, I was ready to write a patch and compile-in this fix into the `refpolicy` itself --- I'm
happy now that I haven't done it as I understand now that it's kind of "dirty" fix.

After more investigation a though came to me (I honestly don't know why it came so late), that the whole deal is with file contexts! And I was right
--- after I assigned `lvm_runtime_t` context to the `/run/cryptsetup` directory with the following command:
```
# semanage fcontext -a -t lvm_runtime_t '/run/cryptsetup(/.*)?'
```
it worked fine on the already running system without previously written module! But, after the reboot, a fiasco awaited for me --- I still got the
same error (`Detected device signature(s) on /dev/sdb1`); and `audit` logs still showed that there is the same denial. But this is where it gets more
interesting --- while failing on the boot, `dmcrypt` succeeds in decryption of all those drives.

After a bit more investigation I realized that the directory `/run/cryptsetup` shouldn't at all be created by the `cryptsetup` itself --- there is
whole `systemd-tmpfiles-setup` service which creates all these directories, including one for the `cryptseup`. A glance on the `rc-update show boot`
showed me that `dmcrypt` service stands near the beginning of the `boot` runlevel, while `systemd-tmpfiles-setup` near the end --- this explains, why
`dmcrypt` succeeded after the `boot` runlevel.

And it seemed that there is a little step to finally ending this problem (I decided to let `OpenRC` queue all those services by its own rather then
explicitly using `rc_after` or `rc_before` --- see [this article](https://wiki.gentoo.org/wiki/User:Eroen/openrc-need-after)):
```bash
# echo "rc_need=dmcrypt" >> /etc/conf.d/localmount
# echo "rc_need=systemd-tmpfiles-setup" >> /etc/conf.d/dmcrypt
```

But things got even worse --- now the whole boot process just stuck. Yet, even more research led me to the fact that there is no
`systemd-tmpfiles-setup` to `rc_need` on, but just `tmpfiles-setup`, which is explicitly stated in the `/etc/init.d/systemd-tmpfiles-setup`:
```bash
    provide tmpfiles-setup
```

Though this "mistype" was kind of block for the whole boot process, the main issues wasn't here. The issue was (and is) in the following dependency
diagram:
```
┌────────────────────────┐   need localmount   ┌────────────┐ need dmcrypt
│ systemd-tmpfiles-setup ├────────────────────►│ localmount ├─────────────┐
└────────────────────────┘                     └────────────┘             │
           ▲                                                              │
           │          need tmpfiles-setup     ┌─────────┐                 │
           └──────────────────────────────────┤ dmcrypt │◄────────────────┘
                                              └─────────┘
```

The current solution was to manually comment `need localmount` in the `/etc/init.d/systemd-tmpfiles-setup`, dooming myself to remember that now I
cannot mount `/run`, for example, inside the `/etc/fstab` --- while I didn't do it previously, it still feels like semi-dirty fix; way better than one
more module for the SELinux or even patch for the `refpolicy`, but still.

So, summing up, the most straightforward way to fix this issue would be:
```bash
# echo "rc_need=dmcrypt" >> /etc/conf.d/localmount
# echo "rc_need=systemd-tmpfiles-setup" >> /etc/conf.d/dmcrypt
# sed -e '/need localmount/s/^/#/' -i /etc/init.d/systemd-tmpfiles-setup
# semanage fcontext -a -t lvm_runtime_t '/run/cryptsetup(/.*)?'
```

I also consider sending a pull request to the [SELinuxProject/refpolicy](https://github.com/SELinuxProject/refpolicy) with the fix for file context.

That's it. Don't mess with SELinux, have a great day.

## UPDATE 23.10.2022
The latest Portage update (3.0.38) now adds file `portage-tmpfiles.conf` into the `tmpfiles.d`, which make the system unbootable. For now I have just
commented this file, but it's actually wacky solution. Maybe need to rethink this whole scheme in the near future or finally file a bug report.
